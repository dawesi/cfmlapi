<!--- $Id: 7_0_2.cfc 14 2011-04-19 15:16:23Z dcepler $ --->
<!---
	Copyright 2009-2011 David C. Epler - http://www.dcepler.net

	This file is part of of the CFML Admin API.

	The CFML Admin API is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The CFML Admin API is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the CFML Admin API.  If not, see <http://www.gnu.org/licenses/>.
--->
<cfcomponent displayname="CFMLAdmin API - Adobe ColdFusion 7.0.2" output="false" hint="CFMLAdmin API Layer for Adobe ColdFusion 7.0.2" extends="base">

	<cffunction name="init" access="public" returntype="any" output="false" hint="">
		<cfargument name="password" type="string" required="true" hint="" />
		<cfargument name="username" type="string" default="" hint="" />
		<cfargument name="adminApiPath" type="string" default="" hint="" />

		<cfset variables.adminPassword = arguments.password />
		<cfset variables.adminUsername = "" />
		<cfset variables.adminApiPath = arguments.adminApiPath />
		<cfreturn this />
	</cffunction>

</cfcomponent>