<!--- $Id: Application.cfc 14 2011-04-19 15:16:23Z dcepler $ --->
<!---
	Copyright 2009-2011 David C. Epler - http://www.dcepler.net

	This file is part of of the CFML Admin API.

	The CFML Admin API is free software: you can redistribute it and/or modify 
	it under the terms of the GNU Lesser General Public License as published by 
	the Free Software Foundation, either version 3 of the License, or 
	(at your option) any later version.

	The CFML Admin API is distributed in the hope that it will be useful, 
	but WITHOUT ANY WARRANTY; without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
	GNU Lesser General Public License for more details.
	
	You should have received a copy of the GNU Lesser General Public License 
	along with the CFML Admin API.  If not, see <http://www.gnu.org/licenses/>.
--->
<cfcomponent displayname="Application" output="false" hint="CFML Admin API Examples">
	
	<cfscript>
		this.name = "CFMLAdminAPI_Examples";
		this.sessionmanagement = true;
	</cfscript>
	
</cfcomponent>