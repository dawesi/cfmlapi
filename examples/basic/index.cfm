<!--- $Id: index.cfm 14 2011-04-19 15:16:23Z dcepler $ --->
<!---
	Copyright 2009-2011 David C. Epler - http://www.dcepler.net

	This file is part of of the CFML Admin API.

	The CFML Admin API is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The CFML Admin API is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the CFML Admin API.  If not, see <http://www.gnu.org/licenses/>.
--->


<cfset request.AdminAPI = createObject("component", "CFMLAdminAPI.cfmladminapi").init("password") />


<!---
 Datasources
--->

<cfdump var="#request.AdminAPI.getDatasources()#" label="before setDatasource()">
<!---
<cfoutput>Datasource "test-mysql5" exists: #request.AdminAPI.datasourceExists("test-mysql5")#</cfoutput><br />

<cfset request.AdminAPI.setDatasource("test-mysql5", "this is a test datasource", "mysql", "litepost", "localhost", "ColdFusion", "password")>
<cfdump var="#request.AdminAPI.getDatasources()#" label="after setDatasource()">
<cfoutput>Datasource "test-mysql5" exists: #request.AdminAPI.datasourceExists("test-mysql5")#</cfoutput><br />

<cfdump var="#request.AdminAPI.getDatasource('test-mysql5')#">

<cfset request.AdminAPI.deleteDatasource("test-mysql5")>
<cfdump var="#request.AdminAPI.getDatasources()#" label="after deleteDatasource()">

<cfset request.AdminAPI.setDatasource(name="test-sqlite-2", description="this is a test datasource", databasetype="jdbc", jdbcURL="jdbc:sqlite:/tmp/test-sqlite.db", jdbcClass="org.sqlite.JDBC")>
<cfdump var="#request.AdminAPI.getDatasources()#" label="after setDatasource(databasetype='jdbc')">

<cfset request.AdminAPI.setDatasource("test-oracle2", "this is a test oracle datasource", "oracle", "xe", "10.211.55.7", "GALLEON", "password")>
<cfdump var="#request.AdminAPI.getDatasources()#" label="after setDatasource(databasetype='oracle')">
--->


<!---
 Mappings
--->
<cfdump var="#request.AdminAPI.getMappings()#" label="before setMapping()">

<!---
<cfoutput>Mapping "/fusebox5" exists: #request.AdminAPI.customTagPathExists('/Library/ColdFusion/Frameworks/Fusebox_5.5.1')#</cfoutput><br />

<cfset request.AdminAPI.setMapping('/fusebox5', '/Library/ColdFusion/Frameworks/Fusebox_5.5.1')>
<cfdump var="#request.AdminAPI.getMappings()#" label="after setMapping()">
<cfoutput>Mapping "/fusebox5" exists: #request.AdminAPI.customTagPathExists('/Library/ColdFusion/Frameworks/Fusebox_5.5.1')#</cfoutput><br />

<cfdump var="#request.AdminAPI.getMapping('fusebox5')#">

<cfset request.AdminAPI.deleteMapping('/fusebox5') />
<cfdump var="#request.AdminAPI.getMappings()#" label="after deleteMapping()">
--->


<!---
 Custom Tag Paths
--->
<cfdump var="#request.AdminAPI.getCustomTagPaths()#" label="before setCustomTagPath()">

<!---
<cfoutput>CustomTag Path "/Library/ColdFusion/CustomTags" exists: #request.AdminAPI.customTagPathExists('/Library/ColdFusion/CustomTags')#</cfoutput><br />

<cfset request.AdminAPI.setCustomTagPath('/Library/ColdFusion/Frameworks')>
<cfdump var="#request.AdminAPI.getCustomTagPaths()#" label="after setCustomTagPath()">
<cfoutput>CustomTag Path "/Library/ColdFusion/CustomTags" exists: #request.AdminAPI.customTagPathExists('/Library/ColdFusion/CustomTags')#</cfoutput><br />

<cfset request.AdminAPI.deleteCustomTagPath('/Library/ColdFusion/CustomTags')>
<cfdump var="#request.AdminAPI.getCustomTagPaths()#" label="after deleteCustomTagPath()">
--->


<!---
 Java CFX
--->
<cfdump var="#request.AdminAPI.getJavaCFXs()#" label="before setJavaCFX()">

<!---
<cfx_hellocoldfusion NAME="CF Dude">
--->

<!---
<cfoutput>Java CFX "cfx_hellocoldfusion" exists: #request.AdminAPI.javaCFXExists('cfx_hellocoldfusion')#</cfoutput><br />

<cfset request.AdminAPI.setJavaCFX('cfx_hellocoldfusion', 'HelloColdFusion', 'has attribute name')>
<cfdump var="#request.AdminAPI.getJavaCFXs()#" label="after setJavaCFX()">
<cfoutput>Java CFX "cfx_hellocoldfusion" exists: #request.AdminAPI.javaCFXExists('cfx_hellocoldfusion')#</cfoutput><br />

<cfdump var="#request.AdminAPI.getJavaCFX('cfx_hellocoldfusion')#">

<cfset request.AdminAPI.deleteJavaCFS('cfx_hellocoldfusion')>
<cfdump var="#request.AdminAPI.getJavaCFXs()#" label="after deleteJavaCFX()">
--->
